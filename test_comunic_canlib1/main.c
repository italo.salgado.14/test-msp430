#include <msp430.h>
#include <stdint.h>
#include "mcp2515_can.h"
#include <stdio.h>
#include <string.h>

/*
 * YO VOY A PEDIR EL DATO
 * 
 * definition de memcopy:
 * void * memcpy ( void * destination, const void * source, size_t num );
 */

/************definition of nodes*********/
#define NUMBER_OF_NODES 3

enum nodes {MOTOR, BMS};
enum var_motor {MOTOR_SPEED};
enum var_bms {BMS_VOLTAGE, BMS_TEMPERATURE};

#define CANID_MYID MOTOR

/*******definition of instructions********/
#define FAIL 1
#define OK 0

#define HLP_INST_REQUEST 0x1
#define HLP_INST_RESPONSE 0x2 //response

/**********def-hash_map-struct*************/

typedef struct _element {
    void *addr;
    size_t len;
} element_t;

typedef struct _node {
    uint32_t id;
    element_t *elements[];
} node_t;

/*************headers**************/
uint8_t hlp_response(can_msg_t* msg_rx, node_t* nodes);
uint8_t hlp_request(uint16_t id_node_request, uint8_t id_data);
uint8_t hlp_response(can_msg_t* msg_rx, node_t* nodes);

/*********definition of functions***********/
uint8_t hlp_process(can_msg_t* msg_rx, node_t* nodes)
{
    switch (msg_rx->data[0]) {
        case HLP_INST_REQUEST:
            hlp_response(msg_rx, nodes);
            break;
        case HLP_INST_RESPONSE:
            memcpy(&nodes[CANID_MYID].elements[msg_tx.data[2]]->addr, msg_tx.data[3], nodes[CANID_MYID].elements[msg_tx.data[2]]->len);
            break;
        default:
            break;
    }
    return 0;
}


uint8_t hlp_request(uint16_t id_node_request, uint8_t id_data)
{
    can_msg_t msg;
    msg.id = id_node_request;
    msg.rtr = 0;
    msg.ext = 0;                    //11 BITS
    msg.len = 3;                    //3 BYTES DATA
    msg.data[0] = HLP_INST_REQUEST;        //INSTRUCTION
    msg.data[1] = CANID_MYID;         //SRC_ID
    msg.data[2] = id_data;
    return can_send(&msg, 3); //IF 0 OK.
}

uint8_t hlp_response(can_msg_t* msg_rx, node_t* nodes)
{
    if (msg_rx->id != CANID_MYID) {
        return FAIL;       // (1) FALLA
    }
    can_msg_t msg_tx;
    msg_tx.id = msg_rx->data[1];   //ID_DESTINATARIO
    msg_tx.rtr = 0;
    msg_tx.data[0] = HLP_INST_RESPONSE;
    msg_tx.data[1] = CANID_MYID;
    msg_tx.data[2] = msg_rx->data[2];

    msg_tx.len = 3 + nodes[CANID_MYID].elements[msg_tx.data[2]]->len; //wat, porque el nodes lleva .
    memcpy(&msg_tx.data[3], nodes[CANID_MYID].elements[msg_tx.data[2]]->addr, nodes[CANID_MYID].elements[msg_tx.data[2]]->len);
    return OK; // todo ok
}


/******init ports*********/
void init_ports()
{
   PAOUT   =   0x0000;
   PASEL   =   0x0000;
   PADIR   =   0xFFFF;

   PBOUT   =   0x0000;
   PBSEL   =   0x0000;
   PBDIR   =   0xFFFF;

   PCOUT   =   0x0000;
   PCSEL   =   0x0000;
   PCDIR   =   0xFFFF;

   PDOUT   =   0x0000;
   PDSEL   =   0x0000;
   PDDIR   =   0x07FF;

   PJDIR   =   0xFFF0; //MODIFICADO
   PJOUT   =   0x0000;
}


/************Init-Variables-(internal)***********/
float motor_speed = 0.0;
uint16_t bms_voltage = 60;
float bms_temperature = 0.0;

//motor
element_t e_motor_speed = {
    &motor_speed, sizeof(motor_speed)
};
node_t n_motor = {MOTOR, &e_motor_speed };

//bms
element_t e_bms[2] = {
    &bms_voltage, sizeof(bms_voltage),
    &bms_temperature, sizeof(bms_temperature)
};
node_t n_bms = { BMS, {&e_bms[0], &e_bms[1]}
};

//all_nodes
node_t *allnodes[] = {&n_bms, &n_motor};

volatile uint8_t timer_flag = 0, can_flag = 0;
volatile uint8_t test = 0;


/***********MAIN*******************/
int main(void)
 {
    WDTCTL = WDTPW | WDTHOLD;
    init_ports();
    P1DIR |= BIT0;
    P1OUT &= ~BIT0;
    can_init(MCP2515_BITRATE_250K);
    can_interrupt_enable(MCP2515_INTERRUPT_TX_ALL | MCP2515_INTERRUPT_RX_ALL | MCP2515_INTERRUPT_ERRIE);
    can_opmode(MCP2515_OPMODE_NORMAL);
    can_enable_rollover();
    test = 0x20;
    test = mcp2515_single_read(MCP2515_CANINTE);
    //int fputc(1, &test);
   // printf("data:%d\n",test );
    TA0CCR0 = 4096; // 1 [s]
    TA0CTL = TASSEL_1 | MC_1 | ID_3;
    TA0CCTL0 |= CCIE;

    while (1) {
        if (!timer_flag && !can_flag) {
            __bis_SR_register(GIE + LPM3_bits);
        }
        if (timer_flag) {
            timer_flag = 0;
            hlp_request(BMS, BMS_VOLTAGE);
        }

        while (can_flag) {
            uint8_t can_int = can_int_handler();
            switch (can_int) {                          //switch de casos: Rx, Tx, Error.
                case MCP2515_INT_RX: {                  // RECEPCION
                    can_msg_t msg_rx;
                    if (!can_receive(&msg_rx)) {
                        P1OUT ^= BIT0;
                    }
                    break;
                }

                case MCP2515_INT_TX_OK:                 // TRANSMISION OK
                  // P1OUT ^= BIT0;
                    //break;

                case MCP2515_INT_ERR_OK:                // RX OVERFLOW ERROR
                    break;

                case MCP2515_INT_ERR:                   // ERROR
                    P1OUT |= BIT0;
                    test = mcp2515_single_read(MCP2515_EFLG);
                    while(1);
                    break;

                default:
                    can_flag = 0;
            }
        }
    }
    return 0;
}



#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR()
{
   TA0CCTL0 &= ~CCIFG;
   timer_flag = 1;
   __bic_SR_register_on_exit(LPM3_bits);
}


#pragma vector = PORT2_VECTOR
__interrupt void P2_ISR() {
   if (P2IFG & MCP2515_INT_BIT) {
       P2IFG &= ~MCP2515_INT_BIT;
       can_flag = 1;
       __bic_SR_register_on_exit(LPM3_bits);
   }
}
