/**
 * @file       uart_printf.h
 * @brief      Archivo cabecera para uart_print.c.
 * @author     Alonso Rodriguez
 * @date       03/07/2017
 * @version    1.0
 */

#ifndef _UART_PRINTF_H_
#define _UART_PRINTF_H_

/*=============================================================================*
||                                INCLUSIONES                                 ||
*=============================================================================*/

#include <msp430.h>
#include <stdio.h>
#include <string.h>

/*=============================================================================*
||                       PROTOTIPOS FUNCIONES GLOBALES                        ||
*=============================================================================*/

/**
 * @brief      Inicializa la comunicacion UART en el modulo USCI configurado.
 * @todo       Baudrate seleccionable.
 * @note       Inicializacion para transmision. No habilita interrupciones.
 */
void uart_init();

/**
 * @brief      Escribe un caracter al stream indicado.
 *
 * @param[in]  c     Caracter a ser escrito.
 * @param      fp    Puntero a FILE representante del stream.
 *
 * @return     Caracter escrito en caso de exito o EOF en caso de error.
 * 
 * @note       Uso interno. Redifinicion de la funcion para redireccionar la
 *             salida de printf() hacia el buffer UART.
 */
int fputc(int c, register FILE *fp);

/**
 * @brief      Escribe un string al stream indicado.
 *
 * @param[in]  ptr   String a ser escrito.
 * @param      fp    Puntero a FILE representante del stream.
 *
 * @return     Valor negativo en caso de exito o EOF en caso de error.
 * 
 * @note       Uso interno. Redefinicion de la funcion para redireccionar la
 *             salida de printf() hacia el buffer UART.
 */
int fputs(const char *ptr, register FILE *fp);

#endif /* _UART_H_ */
