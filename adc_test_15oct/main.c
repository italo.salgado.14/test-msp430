#include <msp430.h>

//breakpoint
//ctrl + click
float adc_to_voltaje(int val_adc){
    float b;
    b=(float)val_adc*3.3 / 4095.0;
    return b;
}


int main(void)
{
  WDTCTL = WDTPW+WDTHOLD;                   // Stop watchdog timer
  P6SEL |= 0x01;                            // Enable A/D channel A0 - activar conversor
  ADC12CTL0 = ADC12ON+ADC12SHT0_2;          // Turn on ADC12, set sampling time
  ADC12CTL1 = ADC12SHP;                     // Use sampling timer
  //ADC12MCTL0 = ADC12SREF_2;                 // Vr+ = VeREF+ (ext) and Vr-=AVss (no esta este pin)

  ADC12CTL0 |= ADC12ENC;                    // Enable conversions
  volatile int data = 0x0000;
  volatile float voltaje = 0x0000;
  while (1)
  {
    ADC12CTL0 |= ADC12SC;                   // Start conversion-software trigger
    while (!(ADC12IFG & BIT0));
    data = ADC12MEM0;
    voltaje = adc_to_voltaje(data);
    __no_operation();                       // SET BREAKPOINT HERE
  }
}


