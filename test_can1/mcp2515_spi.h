/**
 * @file       mcp2515_spi.h
 * @brief      Archivo cabecera para mcp2515_spi.c.
 * @author     Alonso Rodriguez
 * @date       07/07/2017
 * @version    1.2
 */

#ifndef _MCP2515_SPI_H_
#define _MCP2515_SPI_H_

/*=============================================================================*
||                                INCLUSIONES                                 ||
*=============================================================================*/

#include <msp430.h>
#include <stdint.h>

/*=============================================================================*
||                          SET DE INSTRUCCIONES SPI                          ||
*=============================================================================*/

#define MCP2515_READ					0x03
#define MCP2515_RESET					0xC0
#define MCP2515_WRITE					0x02
#define MCP2515_READ_STATUS				0xA0
#define MCP2515_RX_STATUS				0xB0
#define MCP2515_BIT_MODIFY				0x05

// SPI RX STATUS
#define MCP2515_RX_STATUS_NO_MESSAGE	0x00
#define MCP2515_RX_STATUS_RXB0			0x40
#define MCP2515_RX_STATUS_RXB1			0x80
#define MCP2515_RX_STATUS_BOTH			0xC0

#define MCP2515_RX_STATUS_STD			0x00
#define MCP2515_RX_STATUS_STD_RTR		0x08
#define MCP2515_RX_STATUS_EXT			0x10
#define MCP2515_RX_STATUS_EXT_RTR		0x18

#define MCP2515_RX_STATUS_RXB_BITS		0xC0
#define MCP2515_RX_STATUS_EXT_BIT		0x10
#define MCP2515_RX_STATUS_RTR_BIT		0x08

// SPI READ RX BUFFER
#define MCP2515_READ_RX_BUFFER_0_ID		0x90
#define MCP2515_READ_RX_BUFFER_0_DATA	0x92
#define MCP2515_READ_RX_BUFFER_1_ID		0x94
#define MCP2515_READ_RX_BUFFER_1_DATA	0x96

#define MCP2515_READ_RX_BUFFER_BITS     0x90

// SPI LOAD TX BUFFER
#define MCP2515_LOAD_TX_BUFFER_0_ID		0x40
#define MCP2515_LOAD_TX_BUFFER_0_DATA	0x41
#define MCP2515_LOAD_TX_BUFFER_1_ID		0x42
#define MCP2515_LOAD_TX_BUFFER_1_DATA	0x43
#define MCP2515_LOAD_TX_BUFFER_2_ID		0x44
#define MCP2515_LOAD_TX_BUFFER_2_DATA	0x45

#define MCP2515_LOAD_TX_BUFFER_BITS		0x40

// SPI RTS
#define MCP2515_RTS_TXB0				0x81
#define MCP2515_RTS_TXB1				0x82
#define MCP2515_RTS_TXB2				0x84
#define MCP2515_RTS_ALL					0X87

#define MCP2515_RTS_BITS				0x80

// Para can_read_status()
#define MCP2515_STATUS_RX0IF			0x01
#define MCP2515_STATUS_RX1IF			0x02
#define MCP2515_STATUS_TX0REQ			0x04
#define MCP2515_STATUS_TX0IF			0x08
#define MCP2515_STATUS_TX1REQ			0x10
#define MCP2515_STATUS_TX1IF			0x20
#define MCP2515_STATUS_TX2REQ			0x40
#define MCP2515_STATUS_TX2IF			0x80

#define MCP2515_STATUS_TXREQ_BITS		0x54
#define MCP2515_STATUS_RXIF_BITS		0x03
#define MCP2515_STATUS_TXIF_BITS		0xA8

#define MCP2515_TXBDLC_RTR				0x40

/*=============================================================================*
||                       PROTOTIPOS FUNCIONES GLOBALES                        ||
*=============================================================================*/

/**
 * @brief      Inicializa la comunicacion SPI.
 */
void mcp2515_spi_init();

/**
 * @brief      Reinicia los registros internos del CAN controller MCP2515.
 */
void mcp2515_reset();

/**
 * @brief      Lee multiples registros contiguos por SPI.
 *
 * @param[in]  addr      Direccion de inicio de los registros a leer.
 * @param      data_out  Arreglo a contener los datos leidos.
 * @param[in]  len       Numero de registros a leer.
 */
void mcp2515_read(uint8_t addr, uint8_t *data_out, uint8_t len);

/**
 * @brief      Lee registro por SPI.
 *
 * @param[in]  addr  Direccion del registro a leer.
 *
 * @return     Byte con el contenido del registro leido.
 */
uint8_t mcp2515_single_read(uint8_t addr);

/**
 * @brief      Lee buffer de recepcion sin el overhead de una lectura normal.
 *
 * @param      data_out  Arreglo a contener los datos leidos
 * @param[in]  buf       Buffer de recepcion a leer.
 * @param[in]  len       Numero de registros a leer.
 * @param[in]  start     RXBnSIDH (0) o DXBnD0 (1).
 */
void mcp2515_read_rx_buffer(uint8_t *data_out, uint8_t buf, uint8_t len,
							uint8_t start);

/**
 * @brief      Escribe multiples registros contiguos por SPI.
 *
 * @param[in]  addr  Direccion de inicio de los registros a escribir.
 * @param[in]  data  Arreglo de datos a escribir.
 * @param[in]  len   Numero de registros a escribir.
 */
void mcp2515_write(uint8_t addr, uint8_t *data, uint8_t len);

/**
 * @brief      Escribe un registro por SPI.
 *
 * @param[in]  addr  Direccion del registro a escribir
 * @param[in]  data  Byte de datos a escribir.
 */
void mcp2515_single_write(uint8_t addr, uint8_t data);

/**
 * @brief      Carga buffer de transmision sin el overhead de una escritura
 *             normal.
 *
 * @param      data_buf  Arreglo de datos a escribir.
 * @param[in]  buf       Buffer de transmision a escribir.
 * @param[in]  len       Numero de registros a escribir.
 */
void mcp2515_load_tx_buffer(uint8_t *data_buf, uint8_t buf, uint8_t len);

/**
 * @brief      Envia Request To Send para solicitar transmision de los datos
 *             contenidos en el buffer correspondiente.
 *
 * @param[in]  buf   Buffer de transmision a solicitar el envio.
 */
void mcp2515_rts(uint8_t buf);

/**
 * @brief      Entrega informacion sobre el estado del dispositivo.
 *
 * @return     Byte de estado del CAN controller MCP2515.
 */
uint8_t mcp2515_read_status();

/**
 * @brief      Entrega informacion sobre el estado de los buffers de recepcion
 *             del dispositivo.
 *
 * @return     Byte con informacion sobre el estado de los buffers de recepcion.
 */
uint8_t mcp2515_rx_status();

/**
 * @brief      Modifica bits de un registro por SPI.
 *
 * @param[in]  addr  Direccion del registro a escribir.
 * @param[in]  mask  Mascara para modificar solo los bits necesarios.
 * @param[in]  data  Byte contenedor de los bits a modificar.
 *
 * @note       Solo algunos registros son modificables a nivel de bit. Para una
 *             descripción detallada del funcionamiento de la instruccion leer
 *             el datasheet del CAN controller MCP2515.
 */
void mcp2515_bit_modify(uint8_t addr, uint8_t mask, uint8_t data);

#endif /* _MCP2515_SPI_H_ */
