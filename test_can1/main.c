#include <msp430.h>
#include <stdint.h>
#include "mcp2515_can.h"
#include <stdio.h>


void init_ports()

{

   PAOUT   =   0x0000;
   PASEL   =   0x0000;
   PADIR   =   0xFFFF;

   PBOUT   =   0x0000;
   PBSEL   =   0x0000;
   PBDIR   =   0xFFFF;

   PCOUT   =   0x0000;
   PCSEL   =   0x0000;
   PCDIR   =   0xFFFF;

   PDOUT   =   0x0000;
   PDSEL   =   0x0000;
   PDDIR   =   0x07FF;

   PJDIR   =   0xFFF0; //MODIFICADO
   PJOUT   =   0x0000;

}



volatile uint8_t timer_flag = 0, can_flag = 0;
volatile uint8_t test = 0;

int main(void)

{

    WDTCTL = WDTPW | WDTHOLD;



    init_ports();



    P1DIR |= BIT0;

    P1OUT &= ~BIT0;



    can_init(MCP2515_BITRATE_500K);

    can_interrupt_enable(MCP2515_INTERRUPT_TX_ALL | MCP2515_INTERRUPT_RX_ALL | MCP2515_INTERRUPT_ERRIE);

    can_opmode(MCP2515_OPMODE_NORMAL);

    can_enable_rollover();



    test = 0x20;

    test = mcp2515_single_read(MCP2515_CANINTE);



    TA0CCR0 = 4096; // 1 [s]

    TA0CTL = TASSEL_1 | MC_1 | ID_3;

    TA0CCTL0 |= CCIE;



    while (1) {

        if (!timer_flag && !can_flag) {

            __bis_SR_register(GIE + LPM3_bits);

        }

        if (timer_flag) {

            timer_flag = 0;



            can_msg_t msg_tx;



            msg_tx.id = 0x22;

            msg_tx.rtr = 0;

            msg_tx.ext = 0;

            msg_tx.len = 5;

            msg_tx.data[0] = 'H';

            msg_tx.data[1] = 'e';

            msg_tx.data[2] = 'l';

            msg_tx.data[3] = 'l';

            msg_tx.data[4] = 'o';

            can_send(&msg_tx, 3);



        }

        while (can_flag) {

            uint8_t can_int = can_int_handler();

            switch (can_int) {

                case MCP2515_INT_RX: {                  // RECEPCION

                    can_msg_t msg_rx;

                    if (!can_receive(&msg_rx)) {

                        //P1OUT ^= BIT0;

                    }

                    break;

                }

                case MCP2515_INT_TX_OK:                 // TRANSMISION OK

                    P1OUT ^= BIT0;

                    break;

                case MCP2515_INT_ERR_OK:                // RX OVERFLOW ERROR

                    break;

                case MCP2515_INT_ERR:                   // ERROR

                    P1OUT |= BIT0;

                    test = mcp2515_single_read(MCP2515_EFLG);

                    while (1);

                    break;

                default:

                    can_flag = 0;

            }

        }

    }

    return 0;

}



#pragma vector=TIMER0_A0_VECTOR

__interrupt void TIMER0_A0_ISR()

{

   TA0CCTL0 &= ~CCIFG;

   timer_flag = 1;

   __bic_SR_register_on_exit(LPM3_bits);

}



#pragma vector = PORT2_VECTOR

__interrupt void P2_ISR() {

   if (P2IFG & MCP2515_INT_BIT) {

       P2IFG &= ~MCP2515_INT_BIT;

       can_flag = 1;

       __bic_SR_register_on_exit(LPM3_bits);

   }

}
